#!/usr/bin/env bash

ZOLA_version='0.19.2'

if ZOLA="$(type -P zola)" ; then
    if $(${ZOLA} --version | grep -qF " ${ZOLA_version}"); then
	echo "Using system/package based $(${ZOLA} --version)"
    else
	echo "WRONG system/package version $(${ZOLA} --version), expected ${ZOLA_version}."
	ZOLA="podman run --rm -v $PWD:/app:Z --workdir /app ghcr.io/getzola/zola:v${ZOLA_version}"
	echo "Using container based $(${ZOLA} --version)."
    fi
else
    ZOLA="podman run --rm -v $PWD:/app:Z --workdir /app ghcr.io/getzola/zola:v${ZOLA_version}"
    echo "Using container based $(${ZOLA} --version)."
fi

[[ -d ./public ]] && rm -rf ./public

${ZOLA} build && {

for webserver in aspire-7250 m4500; do
    rsync -P -rvzc --include tags --cvs-exclude --delete "public/" "root@${webserver}:/var/www/html/garbaye.fr"
done

}
