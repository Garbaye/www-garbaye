+++
title = "Docs"
date = 2021-04-25
description = "Documentation"
weight = 1
+++

# Documentation Technique
## Équipe technique
L'équipe technique est composée de **Guillaume** et **Loïc**, libristes et adeptes de l'auto-hébergement depuis les années 2000. Ils apprécient aussi la bière.

## Serveurs
Le **serveur principal** des services est une tour x86_64, 4 coeurs, 16GiB RAM équipé de 4TiB de stockage HDD.  
Le **serveur de sauvegarde** est une tour x86_64, 2 coeurs, 4GiB RAM équipé de 2TiB de stockage HDD.  

L'équipe technique a un contrôle total sur ces serveurs : maintenance matérielle, administration système.

## Infrastructure
Le routage réseau est effectué par des routeurs **[OpenWRT](https://openwrt.org/)** avec VLAN "DMZ" isolé.  
Les serveurs utilisent **[Rocky Linux](https://rockylinux.org/)**, avec **[SELinux](https://fr.wikipedia.org/wiki/SELinux)** actif, et sont orchestrés avec **[Ansible](https://fr.wikipedia.org/wiki/Ansible_(logiciel))**.  
Toutes les données sont stockées sur des disques durs chiffrés avec **[LUKS](https://fr.wikipedia.org/wiki/LUKS)**.  
Les services sont gérés dans des containers rootless avec **[podman](https://fr.wikipedia.org/wiki/Podman)**.  
Un serveur web frontal **[Nginx](https://fr.wikipedia.org/wiki/NGINX)** fait l'interface avec les clients.  
Nous utilisons **[WireGuard](https://fr.wikipedia.org/wiki/WireGuard)** comme VPN entre nos serveurs.  
Les sauvegardes quotidiennes sont effectuées selon la stratégie **3-2-1** avec l'outil **[rsnapshot](https://rsnapshot.org/)**:

* 3 copies
* 2 supports différents
* 1 copie hors site

Les services sont supervisés et les administrateurs immédiatement avertis en cas d'indisponibilité.

## Sources
Nos dépots de code sont hébergés sur **[notre forge](https://git.garbaye.fr/Garbaye)**.

## Fournisseurs
Les **courriels et DNS** du domaine **garbaye.fr** sont gérés par **[BookMyName](http://www.bookmyname.com/)**
> BookMyName est le nom commercial de la société Scaleway SAS ex-Online SAS filiale du groupe Iliad.  
> Scaleway SAS, est une société par actions simplifiée au capital de 214 410,50 Euros, filiale du groupe Iliad, immatriculée au Registre du Commerce et des Sociétés de Paris sous le numéro RCS PARIS B 433 115 904, numéro de TVA FR 35 433115904.  
> BookMyName / Scaleway  
> 8 rue de la ville l'Evêque  
> 75008 Paris  
> Téléphone: +33.184130000  

## Objectifs

Les administrateurs poursuivent les objectifs techniques suivants :

* Effectuer une **veille technologique** en suivant les nouveaux outils libres.
* Expérimenter avec de **nouvelles pratiques** du métier. cf. [DevOps](https://fr.wikipedia.org/wiki/Devops).
* Améliorer leur pratique de l'**administration système** : [SSI](https://www.ssi.gouv.fr/administration/bonnes-pratiques/), [KISS](https://fr.wikipedia.org/wiki/Principe_KISS), [IaC](https://fr.wikipedia.org/wiki/Infrastructure_as_code).
* Anticiper les problèmes en prévoyant des **[PRA](https://fr.wikipedia.org/wiki/Plan_de_reprise_d%27activit%C3%A9)** pour divers scénarios d'indisponibilité, avarie, etc.
