+++
title = "À propos"
date = 2021-04-25
description = "À propos"
weight = 1
+++

Ce projet est né d'une réflexion sur l'auto-hébergement, à l'été 2020. **Guillaume et Loïc** administrent pour leur compte des services libres et auto-hébergés depuis de nombreuses années. Quels freins, en 2020, nous empêchent de proposer des services avec une qualité de service acceptable?

Sans pouvoir tout résoudre avec des pirouettes techniques, il nous apparaît aujourd'hui que l'auto-hébergement est une solution plus viable et crédible qu'à nos débuts. [Quelques](https://fr.wikipedia.org/wiki/Let%27s_Encrypt) [digues](https://www.arcep.fr/la-regulation/grands-dossiers-internet-et-numerique/lipv6.html) [techniques](https://www.arcep.fr/cartes-et-donnees/nos-publications-chiffrees/observatoire-des-abonnements-et-deploiements-du-haut-et-tres-haut-debit/observatoire-haut-et-tres-haut-debit-abonnements-et-deploiements-t1-2022.html) ont sautées ces dernières années, alors on se lance et on verra où cela nous mène...

# Nos principes et valeurs
## Logiciel libre
Les [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre), en association aux **licences** et **protocoles libres**, et aux formats de **fichiers ouverts**, sont les outils nécessaire pour protéger nos **droits et libertés**. Garbaye n'utilise que des logiciel libres et participe à leur amélioration.

## Auto-hébergement
L'auto-hébergement consiste à fournir des services numériques directement depuis chez soi, sur un abonnement à Internet classique. Nous adoptons cette pratique dans une démarche de **simplicité, de décentralisation et d'indépendance** vis à vis de tout tiers.

## Impact sur le vivant
Conscients des **impacts du [numérique sur l'environnement](https://ecoinfo.cnrs.fr/)**, et notamment lors de la phase de fabrication de matériel, nous priorisons l'utilisation de matériel de seconde main. Afin de maximiser aussi l'utilisation des ressources à notre disposition, nous évitons d'utiliser des [logiciels usine à gaz](https://fr.wikipedia.org/wiki/Bloatware).

Vous pouvez consulter les choix [techniques](/docs) qui résultent de notre approche.

# Notre structure
## Organisation
Nous sommes actuellement une simple **association de fait** de citoyens, contribuant au projet sur notre temps libre. À ce titre, nos capacités à déployer de nouveaux services ou répondre aux demandes est limitée. Les services disponibles sont ceux listés sur la [page d'accueil](https://garbaye.fr).

## Modèle économique
Du fait de notre structure, les frais de fonctionnement (électricité principalement) sont à notre charge et il n'est pas possible de financer directement **Garbaye**. Si vous avez les moyens financier de soutenir ce type d'initiative, il y a d'autres [CHATONS alentours](https://www.chatons.org/search/near-me) qui sont plus structurés. Vous pouvez évalement soutenir [Framasoft](https://framasoft.org/fr/#support). Nous acceptons les dons de matériel informatique, selon les besoins.

## Public visé
À travers nos services, nous proposons à notre petite échelle des alternatives pour les citoyens désireux de s'émanciper de la mainmise des GAFAM. Les services sont fournis "tel quel" et conviendront mieux à des utilisateurs déjà à l'aise avec l'outil informatique.

N'hésitez pas à [nous contacter](https://garbaye.fr/contact/)!
