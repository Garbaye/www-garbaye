+++
title = "Ajout d'une Forge"
date = 2022-08-13
+++

Nous avons le plaisir de vous annoncer l'ouverture de notre forge [gitea](https://git.garbaye.fr/). L'ouverture de compte sur cette instance est possible sur demande.
<!-- more -->

Il sagit d'un système de gestion et de maintenance collaborative de texte tel que code source, documentation, artistique, littéraire, etc, fournit par [Gitea](https://gitea.io/).
