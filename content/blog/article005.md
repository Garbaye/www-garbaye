+++
title = "Ajout du service Jitsi"
date = 2021-06-23
+++
Nous avons le plaisir de mettre à disposition une intance [Jitsi Meet](https://wiki.chatons.org/doku.php/services/visio-conference/jitsi) ouverte à tous.
<!-- more -->
Il s'agit d'un service de vidéo conférence open source, sécurisé et flexible fourni par la communauté [Jitsi](https://jitsi.org)

Notre service est accessible à l'adresse [jitsi.garbaye.fr](https://jitsi.garbaye.fr)
