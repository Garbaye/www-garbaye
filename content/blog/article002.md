+++
title = "Garbaye est candidat CHATONS"
date = 2021-04-28
+++

Depuis aujourd'hui et dans la poursuite de notre démarche de promotion du libre et de la décentralisation des outils Internet, **Garbaye** est officiellement [candidat](https://framagit.org/chatons/CHATONS/-/issues/172) [CHATONS](https://chatons.org/).
<!-- more -->
> CHATONS est le Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires. Ce collectif vise à rassembler des structures proposant des services en ligne libres, éthiques et décentralisés afin de permettre aux utilisateur⋅ices de trouver rapidement des alternatives respectueuses de leurs données et de leur vie privée aux services proposés par les GAFAM (Google, Apple, Facebook, Amazon, Microsoft). CHATONS est un collectif initié par l'association Framasoft en 2016 suite au succès de sa campagne Dégooglisons Internet.

Ce processus de candidature nous a permis d'identifier des points d'amélioration dans notre démarche. Les délibérations sont prévues pour le 21 juin. D'ici là, d'autres services risquent de pointer le bout de leur nez :-)
