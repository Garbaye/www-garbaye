+++
title = "Status des services"
date = 2021-05-15
+++
Nous avons ajouté une [page de status des services](https://status.garbaye.fr/).
Celle ci est accessible depuis le menu principal du site web.
<!-- more -->
Cette page utilise [tinystatus](https://github.com/bderenzo/tinystatus).
