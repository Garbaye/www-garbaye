+++
title = "Arrêt du service Lutim img.garbaye.fr"
date = 2021-05-15
+++
Le service de partage d'image img.garbaye.fr (Lutim) a été officiellement stoppé après discussion de l'équipe.  
Si vous cherchez à récupérer des images, n'hésitez pas à nous [contacter](pages/contact.html).
<!-- more -->
