+++
title = "Collectif CHATONS"
date = 2021-06-24
+++

Suite à notre [candidature](candidature-chatons.html), le projet Garbaye est fier et honoré aujourd'hui de rejoindre le [collectif CHATONS](https://www.chatons.org/), dont nous partageons les objectifs et valeurs.  
<!-- more -->
Nos services ouverts au public (jitsi, privatebin et date) sont mis à disposition du public à travers la page <https://entraide.chatons.org>.
