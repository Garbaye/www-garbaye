+++
title = "Maintenance site primaire"
date = 2021-10-26
+++
Bonjour,

Une maintenance électrique aura lieu **mercredi 27 octobre 2021, entre 8h et 12h**, au matin et entrainera l'indisponibilité des services :
<!-- more -->
- <https://sondage.garbaye.fr>
- <https://bin.garbaye.fr>
- <https://jitsi.garbaye.fr>
- <https://seafile.garbaye.fr>
- <https://matrix.garbaye.fr>

Le status des services restera disponible à [l'adresse habituelle](https://status.garbaye.fr/)
