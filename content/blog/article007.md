+++
title = "Ajout du service Matrix"
date = 2021-07-15
+++

Nous avons le plaisir de vous annoncer l'ouverture de notre intance [Matrix](https://wiki.chatons.org/doku.php/services/messagerie_instantanee/matrix). L'ouverture de compte sur cette instance est limitée à nos proches.
<!-- more -->
Il s'agit d'un service de méssagerie instanée open source et sécurisé fourni par la fondation [Matrix.org](https://matrix.org)

Venez-nous rejoindre sur [#garbaye:garbaye.fr](https://matrix.to/#/#garbaye:garbaye.fr) :)
