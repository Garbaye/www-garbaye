+++
title = "Nouveau service : “coffre”, un gestionnaire de mot de passe en ligne."
date = 2024-06-29
+++

C'est avec plaisir que nous vous annonçons le lancement de [coffre](https://coffre.garbaye.fr), un service de gestion des mots de passe et toutes vos informations confidentielles. Ce service nécessite une inscription, qui sont actuellement librement ouvertes !

<!-- more -->

Notre utilisation de services en ligne nous oblige à créer toujours plus de comptes informatiques, sur un grand nombre de sites web différents. Ceci nous incite à réutiliser les mêmes mots de passe, ou à les stocker de façon non sécurisée sous forme de simple document, informatiques ou papier. Tout ceci peut facilement être évité grâce à un [gestionnaire de mot de passe](https://fr.wikipedia.org/wiki/Gestionnaire_de_mots_de_passe). Avec un seul mot de passe *maître* à mémoriser, il permet d'utiliser des mots de passe uniques et forts (générés aléatoirement) pour tout service en ligne, ce qui limite les effets de bord lorsqu'un site web se fait compromettre. Une fois familier de l'outil, nous vous recommandons d'activer la [connexion en deux étapes](https://bitwarden.com/fr-fr/help/setup-two-step-login/) pour améliorer grandement la sécurité de votre coffre-fort.

Notre service utilise le logiciel [VaultWarden](https://github.com/dani-garcia/vaultwarden), ce qui le rend compatible avec tous les clients [Bitwarden](https://bitwarden.com) existants : extensions de navigateur web, applications mobiles, etc. L'utilisation est identique à celle de Bitwarden, dont vous trouverez la documentation [en ligne ici](https://bitwarden.com/fr-fr/help/).

Guide de démarrage rapide pour commencer à utiliser notre service :
1. Ouvrir dans un navigateur web [https://coffre.garbaye.fr](https://coffre.garbaye.fr) et **créer un compte**
1. Installer les [extensions de navigateur](https://bitwarden.com/fr-fr/help/getting-started-browserext/) et/ou [applications mobile](https://bitwarden.com/fr-fr/help/getting-started-mobile/) Bitwarden, là où vous allez l'utiliser.
3. Suivre [la documentation](https://bitwarden.com/fr-fr/help/change-client-environment/) afin d'utiliser notre instance **auto-hébergée** https://coffre.garbaye.fr lors de la configuration de ces outils.
4. Se connecter et commencer à utiliser le service.

Quelques informations techniques complémentaires :
* Les coffres fort sont chiffrés par votre *mot de passe maître*, les administrateurs *Garbaye* ne pourront pas vous aider à le récupérer s'il est perdu !
* La fonctionnalité de partage de fichier sécurisée *Bitwarden Send* est désactivée sur notre instance.
* Le service utilise la fonctionnalité PwnedPassword de l'[API Have I Been Pwned](https://haveibeenpwned.com/API/v2#PwnedPasswords), qui transmet une partie du hash du mot de passe afin de déterminer s'il est déjà compromis. Vous pouvez désactiver cela en décochant **Vérifier les brèches de données connues pour ce mot de passe** lors de la création du compte.
* La fonctionnalités de **Rapport sur les fuites de données** est désactivée sur notre instance.
* La fonctionnalité d'**Authentification à deux facteurs** **Clé de sécurité OTP YubiKey** est désactivée sur notre instance -> si vous avez une YubiKey, utilisez **WebAuthn FIDO2**
