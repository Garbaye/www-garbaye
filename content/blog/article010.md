+++
title = "Ouverture de “notif”, un service de notifications."
date = 2022-12-14
+++

Nous lançons aujourd'hui [notif](https://notif.garbaye.fr/), un service de notifications sur navigateur, mobile, tablettes, et pour les plus geeks en ligne de commande avec des simple requêtes HTTP. C'est une instance du logiciel [ntfy](https://ntfy.sh/).

<!-- more -->

Ce service permet en se connectant à un “sujet” (topic), d'échanger des notifications avec tous les périphériques abonnés. Il ne s'agit pas d'une messagerie instantanée, mais d'une alternative aux email, SMS et autre “bots” pour transmettre des notifications en cas d'événement (domotique, alarme, supervision) de façon sûre, en évitant de passer par des tiers qui pourraient en observer le contenu.

Si cet outil vous intéresse, nous le proposons en libre accès. La [documentation officielle (en anglais)](https://notif.garbaye.fr/docs/) est riche et devrait donner des idées à certains. Attention la documentation utilise l'instance officielle `ntfy.sh` comme exemple, pensez à remplacer par notre instance `notif.garbaye.fr` ! Une gestion des utilisateurs et un chiffrement de bout en bout sont en développement pour protéger encore davantage les “sujets” importants.
