+++
title = "Bienvenue sur Garbaye"
weight = 1
+++

Garbaye propose des services numériques libres et éthiques au plus grand nombre. Le mot [Garbaye](https://fr.wikipedia.org/wiki/Garbaye) est une référence à la forêt des Landes de Gascogne.

Les détails du projet sont dans la [page dédiée](/about).

# Liste des services
## Accès libre sans inscription
Service | Lien | Code source | Version
--- | --- | --- | ---
Aide à la prise de RDV | **<https://sondage.garbaye.fr>** | [Framadate](https://framagit.org/framasoft/framadate/framadate) | *1.1.19*
Partage de notes sécurisées | **<https://bin.garbaye.fr>** | [PrivateBin](https://github.com/PrivateBin/PrivateBin/) | *1.7.6*
Visio-conférence | **<https://jitsi.garbaye.fr>** | [Jitsi](https://github.com/jitsi/docker-jitsi-meet) | *10078*
Service de notifications | **<https://notif.garbaye.fr>** | [ntfy](https://github.com/binwiederhier/ntfy) | *2.10.0*

## Accès libre avec inscription
Service | Lien | Code source | Version
--- | --- | --- | ---
Gestionnaire de mots de passe | **<https://coffre.garbaye.fr>** | [VaultWarden](https://github.com/dani-garcia/vaultwarden) | *1.33.2*


## Accès restreint : inscriptions [sur demande](https://garbaye.fr/contact/)
Service | Lien | Code source | Version
--- | --- | --- | ---
Forge projet libres | **<https://git.garbaye.fr>** | [Forgejo](https://codeberg.org/forgejo/forgejo) | *7.0.13*
Messagerie instantanée | **<https://matrix.garbaye.fr>** | [Matrix/Synapse](https://github.com/element-hq/synapse) | *1.125.0*
Stockage de fichiers | **<https://seafile.garbaye.fr>** | [Seafile Server Core](https://github.com/haiwen/seafile-server) | *12.0.10*
