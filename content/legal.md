+++
title = "Mentions Légales"
date = 2021-04-25
description = "Mentions Légales"
weight = 1
+++

# Éditeur

Le directeur de la publication est Guillaume Cassonnet, un des administrateurs système de Garbaye.

Guillaume Cassonnet  
5 rue de la Comete  
33770 Salles

Pour nous contacter par courriel : contact [arobase] garbaye [point] fr

# Hébergement

L'infrastructure physique est hébergée en France, au domicile des administrateurs systèmes de Garbaye, dans les communes de :

* Salles (Gironde)
* Bordeaux (Gironde)

# Limitations contractuelles sur les données techniques

Garbaye ne pourra être tenue responsable de dommages matériels liés à l’utilisation de ses services numériques. De plus, l’utilisateur des services numériques s’engage à utiliser un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour.

Garbaye fournit ses services à titre non professionnel, ce qui implique des risques élevés de pannes ou des dysfonctionnements pouvant s'étaler sur une longue durée. Un usage à titre strictement personnel est donc vivement conseillé. Garbaye ne pourra être tenu responsable en cas de dommages causés par un usage professionnel.
