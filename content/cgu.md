+++
title = "CGU"
date = 2021-04-25
description = "Conditions Générales d'Utilisation"
weight = 1
+++

# Conditions Générales d'Utilisation

## Généralités

Les présentes Conditions Générales d'Utilisation s'appliquent exclusivement à Garbaye et ses services. Garbaye désigne l'infrastructure matérielle, système et logicielle hébergeant les services fournis à l'utilisateur via le domaine garbaye.fr et ses sous-domaines. Les conditions sont sujettes à modification afin de les adapter à des dispositions futures.

## Objet

L’objectif du service numérique fourni par Garbaye est de proposer des services numériques libres, respectueux des utilisateurs. Garbaye adhère pleinement à la [charte](https://www.chatons.org/charte) et au [manifeste](https://www.chatons.org/manifeste) de [CHATONS](https://chatons.org).

## Disponibilité

Garbaye ne garantit pas un accès permanent aux services. Cependant, la disponibilité est assurée au mieux. Les incidents d'indisponibilité des services sont communiqués sur ce présent site web.

## Données personnelles et respect de la vie privée
### Propriété

Garbaye ne s'octroie aucun droit de propriété sur les données personnelles produites par les utilisateurs ou utilisatrices. Les données personnelles regroupent l'ensemble des données liées à une personne à l'exception des journaux systèmes et des relevés métrologiques.

### Utilisation

Les données fournies par les utilisateurs et utilisatrices ne sont utilisées que pour fournir le service lié. En aucun cas une utilisation autre des données personnelles n'est faite telle que leur valorisation commerciale. Seuls les journaux systèmes sont utilisés pour garantir la sécurité de l'infrastructure contre des actions malintentionnées et pour des raisons légales. Les révélés métrologiques permettent le suivi de l'utilisation des ressources de Garbaye et ne peuvent identifier une personne spécifiquement.

### Conservation

Les données personnelles sont conservées pour la durée de provision du service à l'utilisateur ou utilisatrice. Les journaux systèmes sont conservés 31 jours après leur création. Les relevés métrologiques ont une période de rétentions de 365 jours.

### Suppression

L'utilisateur peut, à tout moment, définitivement supprimer ses données personnelles et comptes stockés par Garbaye. Il peut s'agir d'une fonctionnalité fournie par un service ou bien d'une demande adressée à Garbaye.

### Récupération

L'utilisateur ou utilisatrice peut, à tout moment, récupérer ses données personnelles et comptes stockés par Garbaye. Il peut s'agir d'une fonctionnalité fournie par un service ou bien d'une demande adressée à Garbaye.

Note : les CGU sont inspirées de [Exarius](https://exarius.org/legal/). License CC BY-NC 4.0
