+++
title = "Contact"
date = 2021-04-25
description = "Nous contacter"
weight = 1
+++

L'équipe d'administration est joignable par :

* Salon Matrix : **[#garbaye:garbaye.fr](https://matrix.to/#/#garbaye:garbaye.fr)**
* Courriel : **contact [arobase] garbaye [point] fr**
